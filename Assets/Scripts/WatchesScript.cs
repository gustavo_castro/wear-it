using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class WatchesScript : MonoBehaviour
{
    public GameObject watchModel1;
    public GameObject watchModel2;
    public GameObject watchModel3;

    public GameObject w1window;
    public GameObject w2window;
    public GameObject w3window;

    Animation w1WindowAnimation;
    Animation w2WindowAnimation;
    Animation w3WindowAnimation;



    // Start is called before the first frame update
    void Start()
    {
        w1WindowAnimation = w1window.GetComponent<Animation>();
        w2WindowAnimation = w2window.GetComponent<Animation>();
        w3WindowAnimation = w3window.GetComponent<Animation>();


    }

    public void WatchOneButtonClicked()
    {
        // 1) show watch 1 model on user wrist

        watchModel1.SetActive(true);
        watchModel2.SetActive(false);
        watchModel3.SetActive(false);

        // 2) animate watch 1 window

        w1WindowAnimation["windowView1animation"].speed = 1;
        w1WindowAnimation.Play();


    }
    public void WatchTwoButtonClicked()
    {
        // 1) show watch 2 model on user wrist

        watchModel1.SetActive(false);
        watchModel2.SetActive(true);
        watchModel3.SetActive(false);

        // 2) animate watch 2 window

        w2WindowAnimation["windowView2animation"].speed = 1;
        w2WindowAnimation.Play();

    }

    public void WatchThreeButtonClicked()
    {
        // 1) show watch 3 model on user wrist

        watchModel1.SetActive(false);
        watchModel2.SetActive(false);
        watchModel3.SetActive(true);


        // 2) animate watch 3 window


        w3WindowAnimation["windowView3animation"].speed = 1;
        w3WindowAnimation.Play();


    }

    //Funcion para el boton cerrar
    public void CloseButtonClicked()
    {
        string buttonName = EventSystem.current.currentSelectedGameObject.name;
        if (buttonName == "w1close")
        {
            // play animation to close watch1
            w1WindowAnimation["windowView1animation"].speed = -1;
            w1WindowAnimation["windowView1animation"].time = w1WindowAnimation["windowView1animation"].length;
            w1WindowAnimation.Play();


        }
        else if (buttonName == "w2close")
        {
            // play animation to close watch2
            w2WindowAnimation["windowView2animation"].speed = -1;
            w2WindowAnimation["windowView2animation"].time = w2WindowAnimation["windowView2animation"].length;
            w2WindowAnimation.Play();


        }

        else if (buttonName == "w3close")
        {
            // play animation to close watch3
            w3WindowAnimation["windowView3animation"].speed = -1;
            w3WindowAnimation["windowView3animation"].time = w3WindowAnimation["windowView3animation"].length;
            w3WindowAnimation.Play();



        }


    }


}
